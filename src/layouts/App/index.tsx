import React from 'react';
import { Outlet } from 'react-router-dom';
import { Flex } from 'antd';

// Components
import TransformControlSegment from '../../components/TransformControlSegment.tsx';
import ObjectDetailsPanel from '../../components/ObjectDetailsPanel/index.tsx';
import ObjectListPanel from '../../components/ObjectListPanel.tsx';
import ObjectCreateModal from '../../components/ObjectCreateModal.tsx';

export default function App() {
  const layoutStyle: React.CSSProperties = {
    display: 'flex',
    backgroundColor: 'transparent',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100vw',
    height: '100vh',
  };
  const headerStyle: React.CSSProperties = {
    display: 'flex',
    backgroundColor: 'transparent',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100vw',
    top: 20,
    padding: 0,
    zIndex: 200,
  };
  const leftSiderStyle: React.CSSProperties = {
    display: 'flex',
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 0,
    top: 0,
    paddingLeft: 20,
    width: 300,
    zIndex: 200,
  };
  const rightSiderStyle: React.CSSProperties = {
    display: 'flex',
    backgroundColor: 'transparent',
    position: 'absolute',
    right: 0,
    top: 0,
    paddingRight: 20,
    width: 350,
    zIndex: 200,
  };

  return (
    <Flex style={layoutStyle}>
      <ObjectCreateModal />
      <Flex style={headerStyle}>
        <TransformControlSegment />
      </Flex>
      <Flex style={leftSiderStyle}>
        <ObjectListPanel />
      </Flex>
      <Flex style={rightSiderStyle}>
        <ObjectDetailsPanel />
      </Flex>
      <Outlet />
    </Flex>
  );
}





