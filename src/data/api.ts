import axios from 'axios';

// Config
import config from '../config';

const axiosInstance = axios.create({
	baseURL: config.baseApiUrl,
	headers: {
		'Content-Type': 'application/vnd.api+json',
	},
	withCredentials: true,
});

axiosInstance.interceptors.response.use((response) => {
    // NOTE: Any status code that lie within the range of 2xx cause this function to trigger; do something with response data
    return response;
  }, (error) => {
    // NOTE: Any status codes that falls outside the range of 2xx cause this function to trigger; do something with response error

		// Note: Axios doesn't handle 302s. To get around this we're redirecting
		//       with a 403 error and the location in the response
		const redirectLocation = error?.response?.data?.location;
		if (redirectLocation) {
			window.location = redirectLocation;
		} else {
			return Promise.reject(error);
		}
});

const api = (params) => axiosInstance(params).then((response) => {
	let { data } = response;
	if (data) {
		// NOTE: Can modify response data here if needed
	}
	return data;
});

export default api;
