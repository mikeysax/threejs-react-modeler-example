import React from 'react';
import { Flex } from 'antd';

// Components
import WorldViewport from '../../components/WorldViewport.tsx';

export default function Home() {
  return (
    <Flex style={{ height: '100vh', width: '100vw' }}>
      <WorldViewport />
    </Flex>
  );
}
