import React from 'react';
import { createRoot } from 'react-dom/client';
import NotFound from '../NotFound';

it('renders without crashing', () => {
	const div = document.createElement('div');
	createRoot(div).render(<NotFound />);
});
