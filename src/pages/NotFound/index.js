import React, { useEffect } from 'react';

export default function NotFound() {

  useEffect(() => {
    // Note: For redirect if needed
    // window.location.href = '';
  });

  return (
    <div>Not Found</div>
  );
}
