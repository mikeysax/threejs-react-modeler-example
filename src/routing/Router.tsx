import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import _ from 'lodash';

const Router = ({ routes }) => {
  return (
    <BrowserRouter>
      <Routes>
        {_.flatten(routes.map(({ layout: Layout, pages }, index: number) =>
            <Route
              key={`layout-${index}`}
              element={<Layout />}
            >
              {pages.map(({ page: Page, path }, index: number) =>
                <Route
                  key={`page-${index}`}
                  path={path}
                  element={<Page />}
                />
              )}
          </Route>
        ))}
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
