// Layouts
import AppLayout from '../layouts/App/index.tsx';

// Pages
import HomePage from '../pages/Home/index.tsx';
// import NotFoundPage from '../pages/NotFound';

export const paths = {
  home: '/',
  // notFound: '*',
};

export const routeNames = (() => {
  const pathNameObj = {};
  const keys = Object.keys(paths);
  keys.forEach((key) => (pathNameObj[key] = key));
  return pathNameObj;
})();

const routes = [
  {
    layout: AppLayout,
    pages: [
      {
        page: HomePage,
        path: paths.home
      },
      // {
      //   page: NotFoundPage,
      //   path: paths.notFound
      // },
    ],
  },
];

export default routes;
