import React from 'react';
import {
  PlusOutlined,
  EyeOutlined,
  EyeInvisibleOutlined,
} from '@ant-design/icons';
import { Button, Tooltip, List, Flex } from 'antd';

// Components
import Panel from './Panel.tsx';

// Helpers
import { useStore } from '../helpers/useStore.tsx';
import { MeshObjectType } from '../helpers/types.tsx';

export default function ObjectListPanel() {
  const {
    store: { viewportObjects, selectedObjectID },
    setStore: { setViewportObjects, setOpenCreateObjectModal, setSelectedObjectID, setSelectedObjectRef },
  } = useStore();

  const onSelect = (meshId: string) => {
    if (!meshId) return;
    setSelectedObjectID(meshId)
  }

  const onDeselect = () => {
    setSelectedObjectID();
    setSelectedObjectRef();
  }

  const extra = (
    <Tooltip title="Create Object">
      <Button
        shape="circle"
        size="small"
        icon={<PlusOutlined />}
        onClick={() => setOpenCreateObjectModal(true)}
      />
    </Tooltip>
  );

  const VisibilityToggle = (props) => {
    const { meshId, visible } = props;

    const setVisibility = () => {
      const newViewportObjects = viewportObjects.map((meshObject: MeshObjectType) => {
        if (meshObject.meshId === meshId) meshObject.visible = !meshObject.visible;
        return meshObject;
      })
      setViewportObjects(newViewportObjects);
    };

    return (
      <Tooltip title="Toggle Visibility">
        <Button
          type='text'
          shape="circle"
          size="small"
          icon={visible ? <EyeOutlined /> : <EyeInvisibleOutlined />}
          onClick={setVisibility}
        />
      </Tooltip>
    );
  };

  const ListItem = (props) => {
    const { meshObject } = props;
    const { title, meshId, visible } = meshObject;

    const isSelected = selectedObjectID === meshId;

    const onClick = () => {
      if (isSelected) return onDeselect();
      onSelect(meshId);
    }

    const itemStyle: React.CSSProperties = {
      borderRadius: 14,
      padding: 6,
    };

    const buttonStyle: React.CSSProperties = {
      textAlign: 'left',
      width: 240,
      backgroundColor: isSelected ? '#fafafa' : '#fff',
      border: isSelected ? '1px solid rgba(0, 0, 0, 0.2)' : '',
      transition: 'all 0.25s ease-in-out',
    };

    return (
      <List.Item style={itemStyle}>
        <Flex
          align='center'
          style={{ width: '100%' }}
          gap={4}
        >
          <Button
            style={buttonStyle}
            type="text"
            onClick={onClick}
          >
            {title}
          </Button>
          <VisibilityToggle
            key="list-visible"
            meshId={meshId}
            visible={visible}
          />
        </Flex>
      </List.Item>
    );
  };

  const listStyle: React.CSSProperties = {
    background: '#fff',
    borderRadius: 14,
    padding: 12,
    border: '1px solid rgba(0, 0, 0, 0.2)',
    boxShadow: '0 3px 6px rgba(0, 0, 0, 0.02), 0 3px 6px rgba(0, 0, 0, 0.02)',
  };

  return (
    <Panel title="Objects" extra={extra}>
      <List
        itemLayout="horizontal"
        dataSource={viewportObjects}
        renderItem={(item: MeshObjectType) => <ListItem meshObject={item} />}
        style={listStyle}
      />
    </Panel>
  );
}
