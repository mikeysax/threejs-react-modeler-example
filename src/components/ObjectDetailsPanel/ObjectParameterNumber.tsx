import React from 'react';
import { InputNumber } from 'antd';

type Props = {
  value: number;
  labelText?: string;
  onChange: (value: number | null) => void;
  step?: number;
}

export default function ObjectParameterNumber(props: Props) {
  const { value, labelText, onChange, step } = props;

  return (
    <InputNumber
      addonBefore={labelText && <span>{labelText}</span>}
      value={parseFloat(value.toFixed(2))}
      onChange={onChange}
      controls={true}
      step={step || 0.01}
      style={{ width: '100%' }}
    />
  );
}
