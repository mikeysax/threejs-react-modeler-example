import React from 'react';
import { CloseOutlined } from '@ant-design/icons';
import { Tooltip, Button, Input, Flex } from 'antd';

// Components
import Panel from '../Panel.tsx';
import ObjectParameterGroup from './ObjectParameterGroup.tsx';
import ObjectDetailsParameter from './ObjectDetailsParameter.tsx';
import SizeParameter from './SizeParameter.tsx';
import SegmentsParameter from './SegmentsParameter.tsx';
import PositionParameter from './PositionParameter.tsx';
import ScaleParameter from './ScaleParameter.tsx';
import RotationParameter from './RotationParameter.tsx';
import ShadowsParameter from './ShadowsParameter.tsx';
import ColorParameter from './ColorParameter.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';
import ObjectParameterSwitch from './ObjectParameterSwitch.tsx';

export default function ObjectDetailsPanel() {
  const {
    store: { selectedObjectRef },
    setStore: {
      setSelectedObjectID, setSelectedObjectRef
    },
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const geometry = selectedObjectRef?.geometry;
  const material = selectedObjectRef?.material;

  const onClick = () => {
    setSelectedObjectID();
    setSelectedObjectRef();
  };

  const onTitleChange = (event) => {
    const value: string = event.target.value;
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      meshObject.title = value;
      return meshObject;
    });
  };

  const onVisibilityChange = (value: boolean) => {
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      meshObject.visible = value;
      return meshObject;
    })
  }

  const extra = (
    <Tooltip title="Close">
      <Button
        shape="circle"
        size="small"
        icon={<CloseOutlined />}
        onClick={onClick}
      />
    </Tooltip>
  );

  const selectedObject: MeshObjectType = getSelectedObject();

  return (
    selectedObject && (
      <Panel title="Details" extra={extra}>
        <ObjectParameterGroup title="General">
          <Flex vertical>
            <p style={{
              marginRight: 12,
              whiteSpace: 'pre-wrap',
              fontSize: 12
            }} >
              Title
            </p>
            <Input
              title='Title'
              value={selectedObject.title}
              onChange={onTitleChange}
              style={{
                flex: 1,
                width: '100%'
              }}
            />
          </Flex>
          <ObjectParameterSwitch
            labelText='Visibility'
            checked={selectedObject.visible}
            onChange={onVisibilityChange}
          />
        </ObjectParameterGroup>

        {geometry && (
          <ObjectParameterGroup title="Geometry">
            <ObjectDetailsParameter title="Type">
              <p style={{
                fontSize: 12
              }}>
                {geometry.type}
              </p>
            </ObjectDetailsParameter>
            <SizeParameter />
            <SegmentsParameter />
            <ShadowsParameter />
          </ObjectParameterGroup>
        )}

        {material && (
          <ObjectParameterGroup title="Material">
            <ObjectDetailsParameter title="Type">
              <p style={{
                fontSize: 12
              }}>
                {material.type}
              </p>
            </ObjectDetailsParameter>
            <ColorParameter />
          </ObjectParameterGroup>
        )}

        <ObjectParameterGroup title="Transform">
          <PositionParameter />
          <ScaleParameter />
          <RotationParameter />
        </ObjectParameterGroup>
      </Panel>
    )
  );
}

