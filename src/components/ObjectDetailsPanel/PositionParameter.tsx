import React from 'react';

// ThreeJS
import { Vector3 } from 'three';

// Components
import ObjectDetailsParameter from './ObjectDetailsParameter.tsx';
import ObjectParameterNumber from './ObjectParameterNumber.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { CartesianPropertyType, X, Y, Z } from '../../helpers/constants.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';

export default function PositionParameter() {
  const {
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const selectedObject: MeshObjectType = getSelectedObject();

  const onChange = (value: number | null, xyz: CartesianPropertyType) => {
    if (value === null) value = 0;
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      const newSelectedPosition = { ...meshObject.transform.position, [xyz]: value };
      meshObject.transform.position = new Vector3(newSelectedPosition[X], newSelectedPosition[Y], newSelectedPosition[Z]);
      return meshObject;
    });
  };

  return (
    <ObjectDetailsParameter title="Position">
      <ObjectParameterNumber
        value={selectedObject?.transform.position.x || 0}
        labelText={X}
        onChange={(value: number | null) => onChange(value, X)}
      />
      <ObjectParameterNumber
        value={selectedObject?.transform.position.y || 0}
        labelText={Y}
        onChange={(value: number | null) => onChange(value, Y)}
      />
      <ObjectParameterNumber
        value={selectedObject?.transform.position.z || 0}
        labelText={Z}
        onChange={(value: number | null) => onChange(value, Z)}
      />
    </ObjectDetailsParameter>
  );
}
