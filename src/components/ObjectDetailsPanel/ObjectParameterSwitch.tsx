import React from 'react';
import { Flex, Switch } from 'antd';

type Props = {
  checked: boolean;
  labelText?: string;
  onChange: (value: boolean) => void;
}

export default function ObjectParameterSwitch(props: Props) {
  const { checked, labelText, onChange } = props;

  return (
    <Flex>
      {labelText && (
        <p style={{ width: '100%', whiteSpace: 'pre-wrap', fontSize: 12 }} >
          {labelText}
        </p>
      )}
      <Switch
        checked={checked || false}
        onChange={onChange}

      />
    </Flex>
  );
}
