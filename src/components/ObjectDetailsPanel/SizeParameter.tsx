import React from 'react';

// ThreeJS
import { Vector3 } from 'three';

// Components
import ObjectDetailsParameter from './ObjectDetailsParameter.tsx';
import ObjectParameterNumber from './ObjectParameterNumber.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { CartesianPropertyType, X, Y, Z } from '../../helpers/constants.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';

export default function SizeParameter() {
  const {
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const selectedObject: MeshObjectType = getSelectedObject();

  const onChange = (value: number | null, xyz: CartesianPropertyType) => {
    if (value === null) value = 1;
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      const newSelectedSize = { ...meshObject.geometry.size, [xyz]: value };
      meshObject.geometry.size = new Vector3(newSelectedSize[X], newSelectedSize[Y], newSelectedSize[Z]);
      return meshObject;
    }, true);
  };

  return (
    <ObjectDetailsParameter title="Size">
      <ObjectParameterNumber
        value={selectedObject?.geometry.size.x}
        labelText={X}
        onChange={(value: number | null) => onChange(value, X)}
        step={0.25}
      />
      <ObjectParameterNumber
        value={selectedObject?.geometry.size.y}
        labelText={Y}
        onChange={(value: number | null) => onChange(value, Y)}
        step={0.25}
      />
      <ObjectParameterNumber
        value={selectedObject?.geometry.size.z}
        labelText={Z}
        onChange={(value: number | null) => onChange(value, Z)}
        step={0.25}
      />
    </ObjectDetailsParameter>
  );
}

