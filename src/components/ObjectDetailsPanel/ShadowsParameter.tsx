import React from 'react';

// Components
import ObjectDetailsParameter from './ObjectDetailsParameter.tsx';
import ObjectParameterSwitch from './ObjectParameterSwitch.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';

export default function ShadowsParameter() {
  const {
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const selectedObject: MeshObjectType = getSelectedObject();

  const onChange = (parameter: string, value: boolean) => {
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      meshObject.geometry[parameter] = value;
      return meshObject;
    })
  }

  return (
    <ObjectDetailsParameter title="Shadows" vertical={true} >
      <ObjectParameterSwitch
        labelText='Receive Shadow'
        checked={selectedObject?.geometry.receiveShadow}
        onChange={(value: boolean) => onChange('receiveShadow', value)}
      />
      <ObjectParameterSwitch
        labelText='Cast Shadow'
        checked={selectedObject?.geometry.castShadow}
        onChange={(value: boolean) => onChange('castShadow', value)}
      />
    </ObjectDetailsParameter>
  );
}
