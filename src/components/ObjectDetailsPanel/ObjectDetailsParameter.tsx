import React from 'react';
import { Flex, Tooltip } from 'antd';
import { InfoCircleOutlined } from '@ant-design/icons';

type Props = {
  title: string;
  tooltip?: string;
  children?: string | React.JSX.Element | React.JSX.Element[];
  vertical?: boolean;
}

export default function ObjectDetailParameter(props: Props) {
  const { title, tooltip, children, vertical } = props;

  return (
    <>
      <Flex gap={8}>
        <p>{title}</p>
        {tooltip && (
          <Tooltip title={tooltip}>
            <InfoCircleOutlined />
          </Tooltip>
        )}
      </Flex>
      <Flex gap={8} style={{ width: '100%', paddingLeft: 8 }} flex={1} vertical={vertical}>
        {children}
      </Flex>
    </>
  );
}
