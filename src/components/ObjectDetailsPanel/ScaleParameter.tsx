import React from 'react';

// ThreeJS
import { Vector3 } from 'three';

// Components
import ObjectDetailParameter from './ObjectDetailsParameter.tsx';
import ObjectParameterNumber from './ObjectParameterNumber.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { CartesianPropertyType, X, Y, Z } from '../../helpers/constants.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';

export default function ScaleParameter() {
  const {
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const selectedObject: MeshObjectType = getSelectedObject();

  const onChange = (value: number | null, xyz: CartesianPropertyType) => {
    if (value === null) value = 1;
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      const newSelectedScale = { ...meshObject.transform.scale, [xyz]: value };
      meshObject.transform.scale = new Vector3(newSelectedScale[X], newSelectedScale[Y], newSelectedScale[Z]);
      return meshObject;
    });
  };

  return (
    <ObjectDetailParameter title="Scale">
      <ObjectParameterNumber
        value={selectedObject?.transform.scale.x || 1}
        labelText={X}
        onChange={(value: number | null) => onChange(value, X)}
      />
      <ObjectParameterNumber
        value={selectedObject?.transform.scale.y || 1}
        labelText={Y}
        onChange={(value: number | null) => onChange(value, Y)}
      />
      <ObjectParameterNumber
        value={selectedObject?.transform.scale.z || 1}
        labelText={Z}
        onChange={(value: number | null) => onChange(value, Z)}
      />
    </ObjectDetailParameter>
  );
}
