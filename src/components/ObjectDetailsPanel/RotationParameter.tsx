import React from 'react';

// ThreeJS
import { Euler } from 'three';

// Components
import ObjectDetailParameter from './ObjectDetailsParameter.tsx';
import ObjectParameterNumber from './ObjectParameterNumber.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { CartesianPropertyType, X, Y, Z } from '../../helpers/constants.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';

export default function RotationParameter() {
  const {
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const selectedObject: MeshObjectType = getSelectedObject();

  const onChange = (value: number | null, xyz: CartesianPropertyType) => {
    if (value === null) value = 0;
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      const newSelectedRotation = { ...meshObject.transform.rotation, [xyz]: value };
      meshObject.transform.rotation = new Euler(newSelectedRotation[X], newSelectedRotation[Y], newSelectedRotation[Z]);
      return meshObject;
    });
  };

  return (
    <ObjectDetailParameter
      title="Rotation"
      tooltip="Rotation in radians"
    >
      <ObjectParameterNumber
        value={selectedObject?.transform.rotation.x || 0}
        labelText={X}
        onChange={(value: number | null) => onChange(value, X)}
      />
      <ObjectParameterNumber
        value={selectedObject?.transform.rotation.y || 0}
        labelText={Y}
        onChange={(value: number | null) => onChange(value, Y)}
      />
      <ObjectParameterNumber
        value={selectedObject?.transform.rotation.z || 0}
        labelText={Z}
        onChange={(value: number | null) => onChange(value, Z)}
      />
    </ObjectDetailParameter>
  );
}
