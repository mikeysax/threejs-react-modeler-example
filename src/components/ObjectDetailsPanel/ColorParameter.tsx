import React, { useState, useEffect } from 'react';
import { Color } from 'three';
import type { ColorPickerProps } from 'antd/es/color-picker';
import { ColorPicker } from 'antd';

// Components
import ObjectDetailsParameter from './ObjectDetailsParameter.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';

export default function ColorParameter() {
  const {
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const [color, setColor] = useState<string>('');
  const [format, setFormat] = useState<ColorPickerProps['format']>('rgb');

  const selectedObject: MeshObjectType = getSelectedObject();

  useEffect(() => {
    const currentColor: Color | undefined = selectedObject.material.color;
    if (currentColor) {
      setColor(currentColor.getHexString());
    }
  }, []);

  const onChange = (value, hex: string) => {
    setColor(hex);
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      const newColor: Color = new Color(
        value.metaColor.r / 255.0,
        value.metaColor.g / 255.0,
        value.metaColor.b / 255.0
      );
      meshObject.material.color = newColor;
      return meshObject;
    })
  }

  return (
    <ObjectDetailsParameter title="Color">
      <ColorPicker
        format={format}
        value={color}
        onChange={onChange}
        onFormatChange={setFormat}
      />
    </ObjectDetailsParameter>
  );
}
