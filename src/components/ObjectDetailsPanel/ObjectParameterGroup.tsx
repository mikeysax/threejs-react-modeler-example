import React, { useState } from 'react';
import { Flex, Tooltip, Button } from 'antd';
import { UpOutlined, DownOutlined, InfoCircleOutlined } from '@ant-design/icons';

type Prop = {
  title?: string;
  children?: React.JSX.Element | React.JSX.Element[];
  style?: React.CSSProperties,
  tooltip?: string | React.JSX.Element | React.JSX.Element[];
}

export default function ObjectParameterGroup(props: Prop) {
  const { title, children, style, tooltip } = props;

  const [expandedParameterGroup, setExpandedParameterGroup] =
    useState<boolean>(true);

  const toggleExapandedParameterGroup = () =>
    setExpandedParameterGroup(!expandedParameterGroup);

  return (
    <Flex
      vertical
      gap={8}
      style={{
        padding: 16,
        marginBottom: 12,
        borderRadius: 14,
        width: '100%',
        backgroundColor: '#fff',
        border: '1px solid rgba(0, 0, 0, 0.2)',
        boxShadow: '0 3px 6px rgba(0, 0, 0, 0.02), 0 3px 6px rgba(0, 0, 0, 0.02)',
        ...style
      }}
    >
      {title && (
        <Flex
          justify='space-between'
          style={{
            margin: 0,
            marginTop: 2,
            marginBottom: 2,
            minWidth: '50%',
          }}
        >
          <b>{title}</b>
          {tooltip && (
            <>
              {' '}
              <Tooltip title={tooltip}>
                <InfoCircleOutlined />
              </Tooltip>
            </>
          )}
          <Tooltip title={expandedParameterGroup ? 'Collapse' : 'Expand'}>
            <Button
              type="text"
              size="small"
              icon={expandedParameterGroup ? <DownOutlined /> : <UpOutlined />}
              onClick={toggleExapandedParameterGroup}
            />
          </Tooltip>
        </Flex>
      )}
      {expandedParameterGroup && (
        <Flex
          vertical
          gap={8}
          style={{ width: '100%', padding: 8 }}
          flex={1}
        >
          {children}
        </Flex>
      )}
    </Flex>
  );
}
