import React from 'react';

// ThreeJS
import { Vector3 } from 'three';

// Components
import ObjectDetailsParameter from './ObjectDetailsParameter.tsx';
import ObjectParameterNumber from './ObjectParameterNumber.tsx';

// Helpers
import { useStore } from '../../helpers/useStore.tsx';
import { CartesianPropertyType, X, Y, Z } from '../../helpers/constants.tsx';
import { MeshObjectType } from '../../helpers/types.tsx';

export default function SegmentsParameter() {
  const {
    methods: {
      getSelectedObject,
      updateSelectedObject
    }
  } = useStore();

  const selectedObject: MeshObjectType = getSelectedObject();

  const onChange = (value: number | null, xyz: CartesianPropertyType) => {
    if (value === null) value = 1;
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      const newSelectedSegments = { ...meshObject.geometry.segments, [xyz]: value };
      meshObject.geometry.segments = new Vector3(newSelectedSegments[X], newSelectedSegments[Y], newSelectedSegments[Z]);
      return meshObject;
    }, true);
  };

  return (
    <ObjectDetailsParameter title="Segments">
      <ObjectParameterNumber
        value={selectedObject?.geometry.segments.x}
        labelText={X}
        onChange={(value: number | null) => onChange(value, X)}
        step={1.0}
      />
      <ObjectParameterNumber
        value={selectedObject?.geometry.segments.y}
        labelText={Y}
        onChange={(value: number | null) => onChange(value, Y)}
        step={1.0}
      />
      <ObjectParameterNumber
        value={selectedObject?.geometry.segments.z}
        labelText={Z}
        onChange={(value: number | null) => onChange(value, Z)}
        step={1.0}
      />
    </ObjectDetailsParameter>
  );
}

