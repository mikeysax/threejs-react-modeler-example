import React from 'react';
import { Card, Flex } from 'antd';

type Prop = {
  title: string;
  extra: React.JSX.Element | React.JSX.Element[];
  children: React.JSX.Element | React.JSX.Element[];
};

export default function Panel(props: Prop) {
  const { title, extra, children } = props;

  return (
    <Card
      size="small"
      title={title}
      extra={extra}
      style={{
        border: 'transparent',
        backgroundColor: 'transparent',
        width: '100%',
        maxHeight: '100vh',
        overflowY: 'auto',
      }}
      headStyle={{
        position: 'absolute',
        top: 20,
        right: 0,
        fontSize: 18,
        marginBottom: 12,
        width: '100%',
        borderRadius: 14,
        padding: 12,
        backgroundColor: '#fff',
        border: '1px solid rgba(0, 0, 0, 0.2)',
        boxShadow: '0 3px 6px rgba(0, 0, 0, 0.02), 0 3px 6px rgba(0, 0, 0, 0.02)'
      }}
      bodyStyle={{
        padding: 0,
        paddingTop: 85
      }}
    >
      <Flex vertical>
        {children}
      </Flex>
    </Card>
  );
}
