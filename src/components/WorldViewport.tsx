import React from 'react';

// ThreeJS
import { Vector3, Euler } from 'three';
import { Canvas } from '@react-three/fiber';
import {
  PerspectiveCamera,
  OrbitControls,
  Environment,
  Grid,
  TransformControls,
  GizmoHelper,
  GizmoViewport,
} from '@react-three/drei';

// Components
import MeshObject from './MeshObject.tsx';

// Helpers
import { useStore } from '../helpers/useStore.tsx';
import { MeshObjectType } from '../helpers/types.tsx';

export default function WorldViewport() {
  const {
    store: {
      viewportObjects,
      selectedObjectRef,
      transformMode,
      environmentPreset,
    },
    methods: {
      updateSelectedObject
    }
  } = useStore();

  const onTransformControlChange = () => {
    const { position, rotation, scale } = selectedObjectRef;
    updateSelectedObject((meshObject: MeshObjectType): MeshObjectType => {
      meshObject.transform.position = new Vector3(position.x, position.y, position.z)
      meshObject.transform.rotation = new Euler(rotation.x, rotation.y, rotation.z);
      meshObject.transform.scale = new Vector3(scale.x, scale.y, scale.z);
      return meshObject;
    });
  };

  return (
    <Canvas shadows>
      {/* Environment Related Components */}
      <Environment preset={environmentPreset} />
      <PerspectiveCamera
        makeDefault
        position={[0, 1, 6]}
        near={0.5}
        far={500}
      />
      <OrbitControls
        makeDefault
        enableDamping
        enablePan
        enableRotate
        enableZoom
      />
      <GizmoHelper
        alignment="bottom-left"
        margin={[100, 100]}
      >
        <GizmoViewport
          labelColor="white"
          axisHeadScale={1}
        />
      </GizmoHelper>
      <directionalLight
        visible={true}
        castShadow={true}
        intensity={0.9}
        shadow-bias={-0.0004}
        position={[-20, 20, 20]}
      />
      <ambientLight intensity={0.2} />
      <Grid
        infiniteGrid={true}
        fadeDistance={50}
        fadeStrength={0.5}
        sectionThickness={0.5}
        followCamera={true}
      />
      {selectedObjectRef && (
        <TransformControls
          object={selectedObjectRef}
          mode={transformMode}
          onObjectChange={onTransformControlChange}
        />
      )}

      {/* 3D Meshes And Other */}
      {viewportObjects.map((meshObject: MeshObjectType) => (
        <MeshObject key={meshObject.meshId} meshId={meshObject.meshId} meshObject={meshObject} />
      ))}
    </Canvas>
  );
}
