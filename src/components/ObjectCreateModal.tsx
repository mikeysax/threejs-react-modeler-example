import React from 'react';
import { Vector3, Euler, Color } from 'three';
import { Modal, Form, Input, Select } from 'antd';
import _ from 'lodash';

// Helpers
import { useStore } from '../helpers/useStore.tsx';
import { createMeshObject } from '../helpers/createMeshObject.tsx';
import { generateUUID } from '../helpers/generateUUID.tsx';
import {
  GeometryType,
  GeometryOptionType,
  MeshObjectType,
  MaterialType,
  MaterialOptionType
} from '../helpers/types.tsx';
import {
  // GEOMETRY
  BOX_GEOMETRY,
  CAPSULE_GEOMETRY,
  PLANE_GEOMETRY,
  CYLINDER_GEOMETRY,
  CIRCLE_GEOMETRY,
  DODECAHEDRON_GEOMETRY,
  ICOSAHEDRON_GEOMETRY,
  LATHE_GEOMETRY,
  OCTAHEDRON_GEOMETRY,
  POLYHEDRON_GEOMETRY,
  RING_GEOMETRY,
  TETRAHEDRON_GEOMETRY,
  TORUS_GEOMETRY,
  TORUS_KNOT_GEOMETRY,
  TUBE_GEOMETRY,
  SHAPE_GEOMETRY,
  EDGES_GEOMETRY,
  EXTRUDE_GEOMETRY,
  WIREFRAME_GEOMETRY,
  // MATERIAL
  MESH_STANDARD_MATERIAL,
  MESH_TOON_MATERIAL,
  MESH_BASIC_MATERIAL,
  MESH_DEPTH_MATERIAL,
  MESH_DISTANCE_MATERIAL,
  MESH_LAMBERT_MATERIAL,
  MESH_MATCAP_MATERIAL,
  MESH_NORMAL_MATERIAL,
  MESH_PHONG_MATERIAL,
  MESH_PHYSICAL_MATERIAL,
  LINE_BASIC_MATERIAL,
  LINE_DASHED_MATERIAL,
  POINTS_MATERIAL,
  RAW_SHADER_MATERIAL,
  SHADER_MATERIAL,
  SHADOW_MATERIAL,
  // SPRITE_MATERIAL,
} from '../helpers/constants.tsx';

export default function ObjectCreateModal() {
  const {
    store: { openCreateObjectModal },
    setStore: { setOpenCreateObjectModal, setViewportObjects },
  } = useStore();

  const [form] = Form.useForm();

  const onFormSubmit = ({ title, geometry, material }: { title: string, geometry: GeometryType, material: MaterialType }) => {
    const newMeshObject: MeshObjectType = createMeshObject({
      title: title,
      meshId: generateUUID(),
      visible: true,
      geometry: {
        type: geometry,
        size: new Vector3(1.0, 1.0, 1.0),
        segments: new Vector3(16, 16, 16),
        receiveShadow: true,
        castShadow: true,
      },
      material: {
        type: material,
        color: new Color("rgb(150, 150, 150)")
      },
      transform: {
        position: new Vector3(0, 0, 0),
        rotation: new Euler(0, 0, 0),
        scale: new Vector3(1.0, 1.0, 1.0),
      }
    });
    setViewportObjects((data: MeshObjectType[]) => [...data, newMeshObject]);
    form.resetFields();
    setOpenCreateObjectModal(false);
  };

  const onOk = () => {
    form.submit();
  };

  const onCancel = () => {
    setOpenCreateObjectModal(false);
  };

  const geometryOptions: GeometryOptionType[] = [
    BOX_GEOMETRY,
    CAPSULE_GEOMETRY,
    PLANE_GEOMETRY,
    CYLINDER_GEOMETRY,
    CIRCLE_GEOMETRY,
    DODECAHEDRON_GEOMETRY,
    ICOSAHEDRON_GEOMETRY,
    LATHE_GEOMETRY,
    OCTAHEDRON_GEOMETRY,
    POLYHEDRON_GEOMETRY,
    RING_GEOMETRY,
    TETRAHEDRON_GEOMETRY,
    TORUS_GEOMETRY,
    TORUS_KNOT_GEOMETRY,
    TUBE_GEOMETRY,
    SHAPE_GEOMETRY,
    EDGES_GEOMETRY,
    EXTRUDE_GEOMETRY,
    WIREFRAME_GEOMETRY
  ].map(
    (geometry: GeometryType): GeometryOptionType => ({
      value: geometry,
      label: _.startCase(geometry),
    })
  );

  const materialOptions: MaterialOptionType[] = [
    MESH_STANDARD_MATERIAL,
    MESH_TOON_MATERIAL,
    MESH_BASIC_MATERIAL,
    MESH_DEPTH_MATERIAL,
    MESH_DISTANCE_MATERIAL,
    MESH_LAMBERT_MATERIAL,
    MESH_MATCAP_MATERIAL,
    MESH_NORMAL_MATERIAL,
    MESH_PHONG_MATERIAL,
    MESH_PHYSICAL_MATERIAL,
    LINE_BASIC_MATERIAL,
    LINE_DASHED_MATERIAL,
    POINTS_MATERIAL,
    RAW_SHADER_MATERIAL,
    SHADER_MATERIAL,
    SHADOW_MATERIAL,
    // SPRITE_MATERIAL,
  ].map(
    (material: MaterialType): MaterialOptionType => ({
      value: material,
      label: _.startCase(material),
    })
  );

  const onGeometryChange = (value: string) => {
    const titleValue = form.getFieldValue('title');
    if (!titleValue) {
      form.setFieldValue('title', _.startCase(value));
    }
    const materialValue = form.getFieldValue('material');
    if (!materialValue) {
      form.setFieldValue('material', MESH_STANDARD_MATERIAL);
    }
  };

  return (
    <Modal
      title="Create Object"
      open={openCreateObjectModal}
      onOk={onOk}
      onCancel={onCancel}
      okText="Create"
    >
      <Form
        layout="vertical"
        form={form}
        autoComplete="off"
        onFinish={onFormSubmit}
      >
        <Form.Item
          label="Geometry"
          name="geometry"
          rules={[
            {
              required: true,
              message: 'Please select a geometry!',
            },
          ]}
        >
          <Select options={geometryOptions} onChange={onGeometryChange} />
        </Form.Item>
        <Form.Item
          label="Material"
          name="material"
          rules={[
            {
              required: true,
              message: 'Please select a material!',
            },
          ]}
        >
          <Select options={materialOptions} />
        </Form.Item>
        <Form.Item
          label="Title"
          name="title"
          rules={[
            {
              required: true,
              message: 'Please input the object title',
            },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
}
