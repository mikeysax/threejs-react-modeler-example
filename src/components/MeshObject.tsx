import React, {
  useEffect,
  useCallback,
  useRef
} from 'react';

// ThreeJS
import { Wireframe } from '@react-three/drei';

// Helpers
import { useStore } from '../helpers/useStore.tsx';
import { MeshObjectType } from '../helpers/types.tsx';

type Props = {
  meshId: string;
  meshObject: MeshObjectType;
};

export default function MeshObject(props: Props) {
  const { meshId, meshObject, ...otherProps } = props;

  const {
    visible,
    geometry: {
      type: GeometryType,
      size,
      segments,
      receiveShadow,
      castShadow,
    },
    material: {
      type: MaterialType,
      color,
    },
    transform: {
      position,
      rotation,
      scale
    },
  } = meshObject;

  const {
    store: {
      selectedObjectID,
      selectedObjectRef,
      useWireframe,
    },
    setStore: {
      setSelectedObjectID,
      setSelectedObjectRef,
    },
  } = useStore();

  const meshRef: any = useRef();

  const isSelected = selectedObjectID === meshId;

  const updateSelectedObjectRefAndProperties = useCallback(() => {
    if (!isSelected || !meshRef) return;
    if (selectedObjectRef === meshRef.current) return;
    setSelectedObjectRef(meshRef.current);
  }, [meshRef.current, isSelected]);

  useEffect(() => {
    updateSelectedObjectRefAndProperties();
  }, [isSelected, updateSelectedObjectRefAndProperties]);

  const onClick = () => {
    if (isSelected) return;
    setSelectedObjectID(meshId);
  };

  const geometryArgs = () => {
    let args: Array<number> = [];
    if (size) args = [...args, ...size];
    if (segments) args = [...args, ...segments];
    return args
  }

  return (
    <mesh
      ref={meshRef}
      visible={visible}
      receiveShadow={receiveShadow}
      castShadow={castShadow}
      position={position}
      rotation={rotation}
      scale={scale}
      onClick={onClick}
      {...otherProps}
    >
      {GeometryType && (
        <GeometryType args={geometryArgs()} />
      )}
      {MaterialType && (
        <>
          {/* <shadowMaterial transparent opacity={0.4} /> */}
          <MaterialType color={color} />
        </>
      )}
      {isSelected && useWireframe && (
        <Wireframe
          linewidth={0.1}
          thickness={0.025}
        />
      )}
    </mesh>
  );
};
