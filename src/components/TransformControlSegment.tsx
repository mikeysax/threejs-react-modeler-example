import React from 'react';
import { Segmented } from 'antd';
import _ from 'lodash';

// Helpers
import { useStore } from '../helpers/useStore.tsx';
import { TRANSFORM_CONTROL_MODES } from '../helpers/constants.tsx'

export default function TransformControlSegment() {
  const {
    store: { selectedObjectID, transformMode },
    setStore: { setTransformMode },
  } = useStore();

  const onChange = (value) => {
    setTransformMode(value.toLowerCase());
  };

  const segmentStyle: React.CSSProperties = {
    background: '#fff',
    borderRadius: 14,
    padding: 6,
    border: '1px solid rgba(0, 0, 0, 0.2)',
    boxShadow: '0 3px 6px rgba(0, 0, 0, 0.02), 0 3px 6px rgba(0, 0, 0, 0.02)',
  };

  return (
    selectedObjectID && (
      <Segmented
        value={_.capitalize(transformMode)}
        options={TRANSFORM_CONTROL_MODES}
        onChange={onChange}
        style={segmentStyle}
      />
    )
  );
}
