import React from 'react';

// Styles
import './App.css';
import { ConfigProvider } from 'antd';

// Routes
import Router from './routing/Router.tsx';
import routes from './routing/routes.tsx';

// Headers
import Head from './Head.tsx';

// Helpers
import { withProviders } from './helpers/withProvider.tsx';
import { StoreProvider } from './helpers/useStore.tsx';

const AppConfigProvider = ({ children }) => {
  return (
    <ConfigProvider
      theme={{
        token: {
          fontFamily: "'Noto Sans Display', sans-serif",
        }
      }}
    >
      {children}
    </ConfigProvider>
  );
};

const App = () => {
  return (
    <>
      <Head />
      <Router routes={routes} />
    </>
  );
};

export default withProviders(
  AppConfigProvider,
  StoreProvider
)(App);
