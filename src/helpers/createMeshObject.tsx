import { Vector3, Euler, Color } from 'three';
import { MeshObjectType } from './types.tsx';
import { MESH_STANDARD_MATERIAL, BOX_GEOMETRY } from './constants.tsx';
import { generateUUID } from './generateUUID.tsx';

export function createMeshObject(meshObject: MeshObjectType): MeshObjectType {
  return meshObject;
}

export function createDefaultMeshObject(): MeshObjectType {
  return createMeshObject({
    title: 'Default Box',
    meshId: generateUUID(),
    visible: true,
    geometry: {
      type: BOX_GEOMETRY,
      size: new Vector3(1.0, 1.0, 1.0),
      segments: new Vector3(16, 16, 16),
      receiveShadow: true,
      castShadow: true,
    },
    material: {
      type: MESH_STANDARD_MATERIAL,
      color: new Color("rgb(150, 150, 150)")
    },
    transform: {
      position: new Vector3(0, 0, 0),
      rotation: new Euler(0, 0, 0),
      scale: new Vector3(1.0, 1.0, 1.0),
    }
  });
}
