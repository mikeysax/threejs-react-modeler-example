import _ from 'lodash';
import { TransformType, GeometryType, MaterialType, EnvironmentType } from "./types";

// Vector3
export type CartesianPropertyType = 'x' | 'y' | 'z';
export const X: CartesianPropertyType = 'x';
export const Y: CartesianPropertyType = 'y';
export const Z: CartesianPropertyType = 'z';

// Transform Properties
export type TransformProperty = 'position' | 'rotation' | 'scale';
export const POSITION_PROPERTY = 'position';
export const ROTATION_PROPERTY = 'rotation';
export const SCALE_PROPERTY = 'scale';


// Transform
export const TRANSLATE: TransformType = 'translate';
export const ROTATE: TransformType = 'rotate';
export const SCALE: TransformType = 'scale';

export const TRANSFORM_CONTROL_MODES: Array<string> = [TRANSLATE, ROTATE, SCALE].map((mode: TransformType) => _.capitalize(mode));

// Geometry
export const BOX_GEOMETRY: GeometryType = 'boxGeometry';
export const CAPSULE_GEOMETRY: GeometryType = 'capsuleGeometry';
export const PLANE_GEOMETRY: GeometryType = 'planeGeometry';
export const CYLINDER_GEOMETRY: GeometryType = 'cylinderGeometry';
// TODO: Not sure if these geometries are working below
export const CIRCLE_GEOMETRY: GeometryType = 'circleGeometry';
export const DODECAHEDRON_GEOMETRY: GeometryType = 'dodecahedronGeometry';
export const ICOSAHEDRON_GEOMETRY: GeometryType = 'icosahedronGeometry';
export const LATHE_GEOMETRY: GeometryType = 'latheGeometry';
export const OCTAHEDRON_GEOMETRY: GeometryType = 'octahedronGeometry';
export const POLYHEDRON_GEOMETRY: GeometryType = 'polyhedronGeometry';
export const RING_GEOMETRY: GeometryType = 'ringGeometry';
export const TETRAHEDRON_GEOMETRY: GeometryType = 'tetrahedronGeometry';
export const TORUS_GEOMETRY: GeometryType = 'torusGeometry';
export const TORUS_KNOT_GEOMETRY: GeometryType = 'torusKnotGeometry';
// TODO: The below geometry might not be needed or useful
export const TUBE_GEOMETRY: GeometryType = 'tubeGeometry';
export const SHAPE_GEOMETRY: GeometryType = 'shapeGeometry';
export const EDGES_GEOMETRY: GeometryType = 'edgesGeometry';
export const EXTRUDE_GEOMETRY: GeometryType = 'extrudeGeometry';
export const WIREFRAME_GEOMETRY: GeometryType = 'wireframeGeometry';

// Materials
export const MESH_STANDARD_MATERIAL: MaterialType = 'meshStandardMaterial';
export const MESH_TOON_MATERIAL: MaterialType = 'meshToonMaterial';
export const MESH_BASIC_MATERIAL: MaterialType = 'meshBasicMaterial';
export const MESH_DEPTH_MATERIAL: MaterialType = 'meshDepthMaterial';
export const MESH_DISTANCE_MATERIAL: MaterialType = 'meshDistanceMaterial';
export const MESH_LAMBERT_MATERIAL: MaterialType = 'meshLambertMaterial';
export const MESH_MATCAP_MATERIAL: MaterialType = 'meshMatcapMaterial';
export const MESH_NORMAL_MATERIAL: MaterialType = 'meshNormalMaterial';
export const MESH_PHONG_MATERIAL: MaterialType = 'meshPhongMaterial';
export const MESH_PHYSICAL_MATERIAL: MaterialType = 'meshPhysicalMaterial';
export const LINE_BASIC_MATERIAL: MaterialType = 'lineBasicMaterial';
export const LINE_DASHED_MATERIAL: MaterialType = 'lineDashedMaterial';
export const POINTS_MATERIAL: MaterialType = 'pointsMaterial';
export const RAW_SHADER_MATERIAL: MaterialType = 'rawShaderMaterial';
export const SHADER_MATERIAL: MaterialType = 'shaderMaterial';
export const SHADOW_MATERIAL: MaterialType = 'shadowMaterial';
// TODO: Some of these materials error'd out and I'm not sure why
// yet, but it caused an infinite loop
export const SPRITE_MATERIAL: MaterialType = 'spriteMaterial';

// Environments
export const STUDIO_ENVIRONMENT: EnvironmentType = 'studio';
export const APARTMENT_ENVIRONMENT: EnvironmentType = 'apartment';
export const CITY_ENVIRONMENT: EnvironmentType = 'city';
export const DAWN_ENVIRONMENT: EnvironmentType = 'dawn';
export const FOREST_ENVIRONMENT: EnvironmentType = 'forest';
export const LOBBY_ENVIRONMENT: EnvironmentType = 'lobby';
export const NIGHT_ENVIRONMENT: EnvironmentType = 'night';
export const PARK_ENVIRONMENT: EnvironmentType = 'park';
export const SUNSET_ENVIRONMENT: EnvironmentType = 'sunset';
export const WAREHOUSE_ENVIRONMENT: EnvironmentType = 'warehouse';
