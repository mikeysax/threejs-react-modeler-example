import { Color, Euler, Mesh, Vector3 } from "three";

export type TransformType = 'scale' | 'translate' | 'rotate' | undefined;

export type GeometryType =
  | 'boxGeometry'
  | 'capsuleGeometry'
  | 'planeGeometry'
  | 'cylinderGeometry'
  | 'circleGeometry'
  | 'dodecahedronGeometry'
  | 'icosahedronGeometry'
  | 'latheGeometry'
  | 'octahedronGeometry'
  | 'polyhedronGeometry'
  | 'ringGeometry'
  | 'tetrahedronGeometry'
  | 'torusGeometry'
  | 'torusKnotGeometry'
  | 'tubeGeometry'
  | 'shapeGeometry'
  | 'edgesGeometry'
  | 'extrudeGeometry'
  | 'wireframeGeometry'
  | undefined;

export type MaterialType =
  | 'lineBasicMaterial'
  | 'lineDashedMaterial'
  | 'meshBasicMaterial'
  | 'meshDepthMaterial'
  | 'meshDistanceMaterial'
  | 'meshLambertMaterial'
  | 'meshMatcapMaterial'
  | 'meshNormalMaterial'
  | 'meshPhongMaterial'
  | 'meshPhysicalMaterial'
  | 'meshStandardMaterial'
  | 'meshToonMaterial'
  | 'pointsMaterial'
  | 'rawShaderMaterial'
  | 'shaderMaterial'
  | 'shadowMaterial'
  | 'spriteMaterial'
  | undefined;

export type EnvironmentType =
  | 'studio'
  | 'apartment'
  | 'city'
  | 'dawn'
  | 'forest'
  | 'lobby'
  | 'night'
  | 'park'
  | 'sunset'
  | 'warehouse'
  | undefined;

export type MeshObjectType = {
  [x: string]: any;
  meshObject: any;
  title: string;
  meshId: string;
  meshRef?: Mesh;
  geometry: {
    type?: any;
    size?: Vector3;
    segments?: Vector3;
    receiveShadow?: boolean;
    castShadow?: boolean;
  };
  material: {
    type?: MaterialType;
    color?: Color;
  };
  visible: boolean;
  transform: {
    position?: Vector3;
    rotation?: Euler;
    scale?: Vector3;
  }
};

export type GeometryOptionType = {
  value: GeometryType;
  label: string;
};

export type MaterialOptionType = {
  value: MaterialType;
  label: string;
}
