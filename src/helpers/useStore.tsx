import React, { useContext, useState } from 'react';

// ThreeJS
import { Mesh, Vector3, Euler } from 'three';

// Helpers
import { createDefaultMeshObject } from './createMeshObject.tsx';
import {
  TRANSLATE,
  STUDIO_ENVIRONMENT,
} from './constants.tsx';
import {
  TransformType,
  EnvironmentType,
  MeshObjectType,
} from './types.tsx';

type ContextType = {
  store: {
    viewportObjects: MeshObjectType[];
    selectedObjectID: string;
    selectedObjectRef;
    useWireframe: boolean;
    transformMode: TransformType;
    environmentPreset: EnvironmentType;
    openCreateObjectModal: boolean;
  };
  setStore: {
    setViewportObjects: Function;
    setSelectedObjectID: Function;
    setSelectedObjectRef: Function;
    setUseWireframe: Function;
    setTransformMode: Function;
    setEnvironmentPreset: Function;
    setOpenCreateObjectModal: Function;
  },
  methods: {
    findViewportObject: Function;
    getSelectedObject: Function;
    updateSelectedObject: Function;
  }
};

const Context = React.createContext<ContextType>({} as ContextType);

export function useStore() {
  return useContext(Context);
}

export function StoreProvider({ children }) {
  // Application Related
  const [openCreateObjectModal, setOpenCreateObjectModal] = useState<boolean>(false);

  // Selected Object Related
  const [viewportObjects, setViewportObjects] = useState<MeshObjectType[]>([createDefaultMeshObject()]);
  const [selectedObjectID, setSelectedObjectID] = useState<string>('');
  const [selectedObjectRef, setSelectedObjectRef] = useState<Mesh | null | undefined>(null);

  const [useWireframe, setUseWireframe] = useState<boolean>(true);

  // Environment / Tools
  const [transformMode, setTransformMode] = useState<TransformType>(TRANSLATE);
  const [environmentPreset, setEnvironmentPreset] = useState<EnvironmentType>(STUDIO_ENVIRONMENT);

  // Methods
  const findViewportObject = (id: string): MeshObjectType | undefined => (
    viewportObjects.find((meshObj: MeshObjectType) => meshObj.meshId === id)
  );

  const getSelectedObject = (): MeshObjectType | undefined => (
    findViewportObject(selectedObjectID)
  );

  const updateSelectedObject = (callback: (meshObject: MeshObjectType) => MeshObjectType, resetWireframe: boolean = false) => {
    if (resetWireframe) {
      // NOTE: This is a hack to make the wireframe
      // update when changing the segments
      setUseWireframe(false);
      setTimeout(() => setUseWireframe(true), 100);
    }

    const newViewportObjects: Array<MeshObjectType> = viewportObjects.map((meshObject: MeshObjectType) => {
      if (meshObject.meshId === selectedObjectID) meshObject = callback(meshObject);
      return meshObject;
    });
    setViewportObjects(newViewportObjects);
  }

  const value: ContextType = {
    store: {
      viewportObjects,
      selectedObjectID,
      selectedObjectRef,
      useWireframe,
      transformMode,
      environmentPreset,
      openCreateObjectModal,
    },
    setStore: {
      setViewportObjects,
      setSelectedObjectID,
      setSelectedObjectRef,
      setUseWireframe,
      setTransformMode,
      setEnvironmentPreset,
      setOpenCreateObjectModal,
    },
    methods: {
      findViewportObject,
      getSelectedObject,
      updateSelectedObject
    }
  };

  return <Context.Provider value={value}>
    {children}
  </Context.Provider>;
}
